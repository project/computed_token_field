<?php

/**
 * @file
 *
 * This is a FieldWidget for Computed Date fields to provide a default value on the form
 * initially, and override the core widget.
 */

namespace Drupal\computed_token_field\Plugin\Field\FieldWidget;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime\Plugin\Field\FieldWidget\DateTimeDefaultWidget;

/**
 * Plugin implementation of the 'computed_token_date_widget' widget.
 *
 * @FieldWidget(
 *   id = "computed_token_date_widget",
 *   label = @Translation("Computed (visually hidden)"),
 *   field_types = {
 *     "computed_token_date",
 *   }
 * )
 */
class ComputedTokenFieldDateWidget extends DateTimeDefaultWidget {

  /**
   * Rewrite the Default formElement from DateTimeDefaultWidget to provide out computed value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $delta
   * @param array $element
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['value']['#default_value'] = $this->getDefaultValue();
    $element['#type'] = 'hidden';
    $element['#disabled'] = TRUE;
    $element['#description'] = $this->t('Normally this field should not be shown!');
    return $element;
  }

  /**
   * Provide a default value (unix epoch) for computed date fields.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   */
  public function getDefaultValue() {
    $datetime_type = $this->fieldDefinition->getSetting('datetime_type');
    if ($datetime_type === DateTimeItem::DATETIME_TYPE_DATE) {
      $value = '1979-01-01';
      $storage_format = DateTimeItemInterface::DATE_STORAGE_FORMAT;
    } else {
      $value = '1979-01-01T00:00:00';
      $storage_format =  DateTimeItemInterface::DATETIME_STORAGE_FORMAT;

    }
    $date = DrupalDateTime::createFromFormat($storage_format, $value, DateTimeItemInterface::STORAGE_TIMEZONE);
    return $date;
  }

}
