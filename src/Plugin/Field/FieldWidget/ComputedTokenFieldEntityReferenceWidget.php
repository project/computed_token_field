<?php

/**
 * @file
 *
 * This is a FieldWidget for Computed EntityReference fields to provide a default
 * value on the form and override the core widget.
 */

namespace Drupal\computed_token_field\Plugin\Field\FieldWidget;

use Drupal\computed_field\Plugin\Field\FieldWidget\ComputedWidgetBase;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'computed_token_entityreference_widget' widget.
 *
 * @FieldWidget(
 *   id = "computed_token_entityreference_widget",
 *   label = @Translation("Computed (visually hidden)"),
 *   field_types = {
 *     "computed_token_entityreference",
 *   }
 * )
 */
#class ComputedTokenFieldEntityReferenceWidget extends EntityReferenceAutocompleteWidget {
class ComputedTokenFieldEntityReferenceWidget extends OptionsSelectWidget {
  // @TODO These methods provide the functionality from ComputedWidgetBase, which should probably move into a Trait.
  /**
   * The default value.
   *
   * @var string
   */
  public $default_value = '';

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('This field should be in the visible area if it was added to the field list after content has been created for this bundle. You can save all those contents to apply the computed value and then safely move this field to the disabled area.');
    return $summary;
  }

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['value'] = $element + [
        '#title' => $this->fieldDefinition->getName(),
        '#type' => 'hidden',
        '#default_value' => NULL,
        '#disabled' => TRUE,
        '#description' => $this->t('Normally this field should not be shown!'),
      ];
    return $element;
  }

}
