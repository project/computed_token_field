<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'computed_string' field type.
 *
 * @FieldType(
 *   id = "computed_token_entityreference",
 *   label = @Translation("Computed Token (entity reference)"),
 *   description = @Translation("This field defines a Entity Reference field whose value is computed by Token"),
 *   category = @Translation("Computed"),
 *   default_widget = "computed_token_entityreference_widget",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 *   provider = "computed_field",
 * )
 */
class ComputedTokenFieldEntityReferenceItem extends EntityReferenceItem {
  use ComputedTokenFieldItemTrait {
    executeCode as baseExecuteCode;
    fieldSettingsForm as baseFieldSettingsForm;
  }
//  use StringTranslationTrait;

  /**
   * Determines whether the item holds an unsaved entity.
   *
   * This is notably used for "autocreate" widgets, and more generally to
   * support referencing freshly created entities (they will get saved
   * automatically as the hosting entity gets saved).
   *
   * @return bool
   *   TRUE if the item holds an unsaved entity.
   */
  public function hasNewEntity() {
    return !$this->isEmpty() && $this->target_id === NULL && isset($this->entity) && $this->entity->isNew();
  }

  public function executeCode() {
    $value = $this->baseExecuteCode();

    if (!is_numeric($value)) {
      $value = NULL;
    }

    $this->setValue(['target_id' => $value]);
    return $value;
  }

  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $element += $this->baseFieldSettingsForm($form, $form_state);
    return $element;
  }
}
