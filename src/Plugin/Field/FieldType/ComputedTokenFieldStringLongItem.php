<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\computed_field\Plugin\Field\FieldType\ComputedStringLongItem;

use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Plugin implementation of the 'computed_string' field type.
 *
 * @FieldType(
 *   id = "computed_string_long",
 *   label = @Translation("Computed Token (text, long)"),
 *   description = @Translation("This field defines a long text field whose value is computed by Token"),
 *   category = @Translation("Computed"),
 *   default_widget = "computed_string_widget",
 *   default_formatter = "computed_string",
 *   provider = "computed_field"
 * )
 */
class ComputedTokenFieldStringLongItem extends ComputedTokenFieldStringItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();
    $schema = [
      'columns' => [
        'value' => [
          'type' => $settings['case_sensitive'] ? 'blob' : 'text',
          'size' => 'big',
        ],
      ],
    ];

    return $schema;
  }

}
