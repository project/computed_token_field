<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Plugin implementation of the 'computed_string' field type.
 *
 * @FieldType(
 *   id = "computed_token_date",
 *   label = @Translation("Computed Token (date)"),
 *   description = @Translation("This field defines a date field whose value is computed by Token"),
 *   category = @Translation("Computed"),
 *   default_widget = "computed_token_date_widget",
 *   default_formatter = "datetime_default",
 *   provider = "computed_field",
 * )
 */
class ComputedTokenFieldDateTimeItem extends DateTimeItem {
  use ComputedTokenFieldItemTrait {
    executeCode as baseExecuteCode;
  }

  public function executeCode() {
    $token_value = $this->getSetting('token_value');
    $value = $this->baseExecuteCode();

    // If the token replacement did nothing, set the value to NULL.
    if ($value === $token_value) {
      $value = NULL;
    }

    return $value;
  }

}
