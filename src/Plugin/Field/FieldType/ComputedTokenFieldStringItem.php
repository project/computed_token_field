<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Plugin implementation of the 'computed_string' field type.
 *
 * @FieldType(
 *   id = "computed_token_string",
 *   label = @Translation("Computed Token (text)"),
 *   description = @Translation("This field defines a text field whose value is computed by Token"),
 *   category = @Translation("Computed"),
 *   default_widget = "computed_string_widget",
 *   default_formatter = "computed_string",
 *   provider = "computed_field"
 * )
 */
class ComputedTokenFieldStringItem extends ComputedTokenFieldStringItemBase {
  use ComputedTokenFieldItemTrait;
  use StringTranslationTrait;

  /* NOTE:
   * The 3 methods below are copied from computed_field!Plugin!Field!FieldType!ComputedStringItem.
   * Because we inherit from our own Base which extends ComputedStringBase, we need to re-implement
   * these here so the data storage works properly.
   */

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 255,
      'is_ascii' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $settings = $field_definition->getSettings();
    $schema = [
      'columns' => [
        'value' => [
          'type' => $settings['is_ascii'] === TRUE ? 'varchar_ascii' : 'varchar',
          'length' => (int) $settings['max_length'],
          'binary' => $settings['case_sensitive'],
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $settings = $this->getSettings();
    $element = [];

    $element['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#default_value' => $settings['max_length'],
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $element;
  }

  public function executeCode() {
    $value = parent::executeCode();
    if (empty($value)) {
      return $value;
    }
    return mb_substr($value, 0, $this->getSettings()['max_length'] - 1);
  }
}
