<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\computed_field\Plugin\Field\FieldType\ComputedFieldItemTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Common methods for Computed Token Field FieldType plugins.
 *
 * The FieldType plugins in this module descend from either FieldItemBase
 * (numbers via ComputedFieldItemBase) or StringItemBase (strings via
 * ComputedStringItemBase). As they have no common ancestry outside of Core,
 * it's necessary to introduce this trait to prevent code duplication across
 * hierarchies.
 */
trait ComputedTokenFieldItemTrait {

  use StringTranslationTrait;
  use ComputedFieldItemTrait;

  /**
   * Performs the field value computation.
   *
   * If this method is being overridden to return a typed result, the class must
   * use ComputedFieldStronglyTypedItemTrait to ensure access to raw results.
   *
   * @see ComputedFieldStronglyTypedItemTrait
   */
  public function executeCode() {
    $entity = $this->getEntity();
    $field = $this->getFieldDefinition();
    $name = $field->getName();

    // This probably needs the context of a specific field instance?
    $token_value = $this->getSetting('token_value');
    return \Drupal::token()->replace($token_value, ['node' => $entity]);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'token_value' => '',
      ] + parent::defaultFieldSettings();
  }

  /**
   * Default field settings form.
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $settings = $this->getSettings();
    $element['token_value'] = [
      '#type' => 'textfield',
      '#maxlength' => 512,
      '#title' => $this->t("Token"),
      '#default_value' => $settings['token_value'],
      '#description' => $this->t("Enter text including tokens which will be replaced to compute the value of this field."),
      '#required' => TRUE,
    ];
    $element['token_help'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => 'all',
    ];
    return $element;
  }

}
