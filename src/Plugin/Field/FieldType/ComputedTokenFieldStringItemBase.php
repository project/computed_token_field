<?php

namespace Drupal\computed_token_field\Plugin\Field\FieldType;

use Drupal\computed_field\Plugin\Field\FieldType\ComputedFieldItemTrait;
use Drupal\computed_field\Plugin\Field\FieldType\ComputedStringItemBase;
use Drupal\computed_field\Plugin\Field\FieldType\ComputedStringItem;
use Drupal\computed_field\Plugin\Field\FieldType\ComputedStringLongItem;

use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin base of the computed token string field type.
 */
abstract class ComputedTokenFieldStringItemBase extends ComputedStringItemBase {
  use ComputedTokenFieldItemTrait {
    executeCode as baseExecuteCode;
  }

  public function executeCode() {
    $token_value = $this->getSetting('token_value');
    $value = $this->baseExecuteCode();

    if ($token_value === $value) {
      $value = '';
    }

    return $value;
  }
}
