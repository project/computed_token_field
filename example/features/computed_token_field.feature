@api @computed-token-field @javascript @disabled
Feature: Computed Token Fields work.
  In order to compute tokens on nodes.
  As a Drupal user
  I need to be able to create and view computed token fields.

  Background:
    Given I am logged in as an "Administrator"

  Scenario: Create a node with ECK parts and subparts
    When I go to "/node/add/computed_token_field_node"
     And I fill in "Computed Token Node" for "Title"
     And I fill in "Main part title" for "Main entity title"
     And I fill in "Main part text goes here" for "Main part entity text field"
     And I enter "01/19/2024" for "edit-field-main-part-values-0-inline-entity-form-field-ctfe-main-part-entity-date-0-value-date"
     And I select "Anonymous" from "Main part entity user field"
     And I fill in "Sub part title" for "Sub entity title"
     And I fill in "Sub part text" for "Subpart entity text field"
     And I fill in "01/22/2024" for "edit-field-main-part-values-0-inline-entity-form-field-main-subpart-ref-0-inline-entity-form-field-ctfe-subpart-entity-date-0-value-date"
     And I select "Anonymous" from "Subpart entity user field"
     And I press "Save"
    Then I should see "Computed Token Field Example Nodetype Computed Token Node has been created."
    Then I should see "Computed Token Node" in the "Page Title" region
      # Main part: stanza for each of 3 field types, checking we see only the computed variant.
     And I should see "Computed main part text"
     And I should see "Main part text goes here"
     And I should not see "Main part entity text field"
     And I should see "Computed main part date"
     And I should see "2024-01-19"
     And I should not see "Main part entity date field"
     And I should see "Computed main part reference"
     And I should see "Anonymous"
     And I should not see "Main part entity user field"
     # Sub part:
     And I should see "Computed sub part text"
     And I should see "Sub part text"
     And I should not see "Subpart entity text field"
     And I should see "Computed sub part date"
     And I should see "2024-01-22"
     And I should not see "Subpart entity date field"
     And I should see "Computed sub part user field"
     And I should see "Anonymous"
     And I should not see "Subpart entity user field"

  Scenario: Create a node with ECK parts and subparts with empty values
    When I go to "/node/add/computed_token_field_node"
    And I fill in "Computed Token Node with empty values" for "Title"
    And I fill in "Main part title" for "Main entity title"
    # Leave these values empty in this case, to prove they don't show up in the resulting computed fields.
    #And I fill in "Main part text goes here" for "Main part entity text field"
    #And I enter "01/19/2024" for "edit-field-main-part-values-0-inline-entity-form-field-ctfe-main-part-entity-date-0-value-date"
    #And I select "Anonymous" from "Main part entity user field"
    And I fill in "Sub part title" for "Sub entity title"
    And I fill in "Sub part text" for "Subpart entity text field"
    And I fill in "01/22/2024" for "edit-field-main-part-values-0-inline-entity-form-field-main-subpart-ref-0-inline-entity-form-field-ctfe-subpart-entity-date-0-value-date"
    And I select "dev" from "Subpart entity user field"
    And I press "Save"
    Then I should see "Computed Token Field Example Nodetype Computed Token Node with empty values has been created."
    Then I should see "Computed Token Node with empty values" in the "Page Title" region
      # Main part: stanza for each of 3 field types, checking we see only the computed variant.
    And I should not see "Computed main part text"
    And I should not see "Main part text goes here"
    And I should not see "Main part entity text field"
    And I should not see "Computed main part date"
    And I should not see "2024-01-19"
    And I should not see "Main part entity date field"
    And I should not see "Computed main part reference"
    And I should not see "Anonymous"
    And I should not see "Main part entity user field"
     # Sub part:
    And I should see "Computed sub part text"
    And I should see "Sub part text"
    And I should not see "Subpart entity text field"
    And I should see "Computed sub part date"
    And I should see "2024-01-22"
    And I should not see "Subpart entity date field"
    And I should see "Computed sub part user field"
    And I should see "dev"
    And I should not see "Subpart entity user field"
