<?php

use Drupal\field\FieldStorageConfigInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function computed_token_field_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage) {
  if ($field_storage->getType() == "computed_token_date") {
    // Call helper function from core that allows this field to be treated as a date (this should be based on the field type)
    $data = datetime_type_field_views_data_helper($field_storage, [], $field_storage->getMainPropertyName());
  }
}
